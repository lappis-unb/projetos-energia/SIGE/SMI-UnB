from django_cron import CronJobBase, Schedule

from .utils import DataCollector


class DataCollectCronJob(CronJobBase):
    RUN_EVERY_MINS = 0
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'smi_unb.data_reader.cronjob.DataCollectCronJob'

    def do(self):
        data_collector = DataCollector()
        data_collector.perform_all_data_collection()
