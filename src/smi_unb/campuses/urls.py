from django.conf.urls import url

from . import views

app_name = 'campuses'
# FIXME
urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(
        r'^(?P<campus_string>[\w\-]+)/$',
        views.campus_index,
        name="campus_index"
    ),
    url(
        r'^(?P<campus_string>[\w\-]+)/info/$',
        views.campus_info,
        name="campus_info"
    ),
    url(
        r'^(?P<campus_string>[\w\-]+)/desativados/$',
        views.inactive_buildings,
        name="inactive_buildings"
    ),
]
