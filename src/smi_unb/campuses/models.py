from django.db import models


class AdministrativeRegion(models.Model):
    name = models.CharField(max_length=50)


class Campus(models.Model):
    administrative_region = models.ForeignKey(AdministrativeRegion,
                                              on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=50)
    acronym = models.CharField(max_length=20)
    address = models.CharField(max_length=150)
    phone = models.CharField(max_length=30)
    url_param = models.CharField(max_length=15, unique=True)
    website_address = models.CharField(max_length=100)
    active = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def get_all_active_buildings(self):
        return self.building_set.filter(active=True)

    def get_all_inactive_buildings(self):
        return self.building_set.filter(active=False)
