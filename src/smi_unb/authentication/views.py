from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import logout
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import LoginForm


def make_login(request):
    teste = request
    next_page = request.POST.get('next', request.GET.get('next', ''))

    if request.user.is_authenticated:
        if next_page:
            return HttpResponseRedirect(next_page)
        else:
            return HttpResponseRedirect(reverse('dashboard'))
    else:
        if request.POST:
            form = LoginForm(request.POST)

            if form.is_valid():
                email = form.cleaned_data['email']
                password = form.cleaned_data['password']

                user = authenticate(username=email, password=password)

                if user is not None and user.is_active:
                    login(teste, user)

                    if next_page:
                        return HttpResponseRedirect(next_page)
                    else:
                        return HttpResponseRedirect(reverse('dashboard'))
        else:
            form = LoginForm()

    return render(request, "users/login.html", {'form': form})


@login_required
def make_logout(request, *args, **kwargs):
    kwargs['next_page'] = reverse('index')

    return logout(request, *args, **kwargs)
