function load_permissions(){
    if(document.getElementById("id_user_type_1").checked == true) {
        selectAdminPermissions();
    }
}

function selectUserPermissions(){
    $("#id_permissions_codenames :input").prop("disabled", false);
    $("#id_permissions_codenames :input").prop("checked", false);
}

function selectAdminPermissions(){
    $("#id_permissions_codenames :input").prop("disabled", true);
    $("#id_permissions_codenames :input").prop("checked", true);
}