from django import forms
from django.utils.translation import ugettext as _

from smi_unb.transductor.models import Transductor


class NewGraphForm(forms.Form):
    MEASUREMENTS_TYPES = [
        ('voltage', 'Tensão (V)'),
        ('current', 'Corrente (A)'),
        ('active_power', 'Potência Ativa (kW)'),
        ('reactive_power', 'Potência Reativa (kVAr)'),
        ('apparent_power', 'Potência Aparente (kVA)')
    ]

    MEASUREMENT_DAYS = [
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
    ]

    measurement_type = forms.ChoiceField(
        label="Tipo de Medição",
        choices=MEASUREMENTS_TYPES,
        widget=forms.Select(
            attrs={
                'class': 'selectpicker form-control',
                'data-live-search': 'true'
            }
        ),
        initial='voltage'
    )

    transductor = forms.ModelChoiceField(
        label="Transdutor",
        queryset=Transductor.objects.all(),
        widget=forms.Select(
            attrs={
                'class': 'selectpicker form-control',
                'data-live-search':
                'true'
            }
        ),
        required=True,
        initial=0
    )

    current_measurement = forms.BooleanField(required=False)

    past_measurement_day = forms.ChoiceField(
        label="Medições de Dias Anteriores",
        choices=MEASUREMENT_DAYS,
        widget=forms.RadioSelect,
        required=False,
    )

    manual_initial_date = forms.DateTimeField(
        input_formats=['%d/%m/%Y %H:%M'],
        label="Data Inicial",
        widget=forms.DateTimeInput(
            attrs={
                'class': 'form-control',
                'type': 'text',
                'id': 'id_date_1'
            }
        ),
        required=False
    )

    manual_final_date = forms.DateTimeField(
        input_formats=['%d/%m/%Y %H:%M'],
        label="Data Final",
        widget=forms.DateTimeInput(
            attrs={
                'class': 'form-control',
                'type': 'text',
                'id': 'id_date_2'
            }
        ),
        required=False
    )

    def is_valid(self):
        valid = super(NewGraphForm, self).is_valid()

        if not valid:
            return valid

        current_measurement = self.cleaned_data['current_measurement']
        past_day = self.cleaned_data['past_measurement_day']
        initial_date = self.cleaned_data['manual_initial_date']
        final_date = self.cleaned_data['manual_final_date']

        params = [current_measurement, past_day, initial_date, final_date]
        total_params = 0
        max_graph_days = 7

        for param in params:
            if param:
                total_params += 1

        if total_params == 0:
            self.add_error(
                None,
                _('Nenhuma opção informada.')
            )
            return False
        else:
            if total_params == 1 and (initial_date or final_date):
                if initial_date and not final_date:
                    self.add_error(
                            None,
                            _('Campo de Data Final não foi informado.')
                        )
                    return False
                else:
                    self.add_error(
                            None,
                            _('Campo de Data Inicial não foi informado.')
                        )
                    return False
            elif total_params > 1:
                if not (final_date and initial_date):
                    self.add_error(
                        None,
                        _('Muitas opções escolhidas' +
                          ' ao mesmo tempo.')
                    )
                    return False
                elif final_date <= initial_date:
                    self.add_error(
                        None,
                        _('Data final não pode vir antes' +
                          ' ou ser igual a data inicial.')
                    )
                    return False
                elif (final_date - initial_date).days > max_graph_days:
                    self.add_error(
                        None,
                        _('Período informado muito longo.')
                    )
                    return False
        return True
