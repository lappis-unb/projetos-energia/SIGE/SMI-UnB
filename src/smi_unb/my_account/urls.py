from django.conf.urls import url

from . import views

app_name = 'my_account'
urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^editar/$', views.self_edit, name="self_edit"),
    url(r'^mudar_senha/$', views.change_password, name="change_password"),
]
