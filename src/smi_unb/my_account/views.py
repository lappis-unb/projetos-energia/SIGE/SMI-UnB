from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

from .forms import ChangePasswordForm, SelfEditForm


@login_required
def index(request):
    return render(request, 'my_account/index.html')


@login_required
def self_edit(request):
    user = get_object_or_404(User, pk=request.user.id)

    if request.POST:
        form = SelfEditForm(request.POST, instance=user)

        if form.is_valid():
            if form.has_changed():
                form.save()

                messages.success(
                    request,
                    'Suas informações foram alteradas com sucesso!'
                )

                return HttpResponseRedirect(reverse('my_account:index'))
            else:
                form.add_error(None, "Nenhuma Mudança Encontrada.")
    else:
        form = SelfEditForm(instance=user)

    return render(request, 'my_account/self_edit.html', {'form': form})


@login_required
def change_password(request):
    if request.POST:
        form = ChangePasswordForm(request.user, request.POST)

        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Sua senha foi alterada com sucesso!')

            return HttpResponseRedirect(reverse('my_account:index'))
    else:
        form = ChangePasswordForm(request.user)

    return render(request, 'my_account/change_password.html', {'form': form})
