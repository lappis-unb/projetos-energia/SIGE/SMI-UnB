from django.contrib.auth.models import User
from django.test import TestCase

from smi_unb.my_account.forms import ChangePasswordForm, SelfEditForm


class UsersFormTest(TestCase):
    def setUp(self):
        self.superuser = User(
            username='test',
            first_name='superfirst',
            last_name='superlast',
            email="admin@admin.com",
            is_superuser=True,
        )
        self.superuser.set_password('12345')
        self.superuser.save()

    def test_correct_change_password_form(self):
        data = {
            'old_password': '12345',
            'new_password1': 'anotherpassword',
            'new_password2': 'anotherpassword',
        }

        form = ChangePasswordForm(self.superuser, data)
        self.assertTrue(form.is_valid())

    def test_correct_self_edit_form(self):
        data = {
            'first_name': 'firstname',
            'last_name': 'lastname',
        }

        form = SelfEditForm(instance=self.superuser, data=data)
        self.assertTrue(form.is_valid())
