import mock
import requests

from django.test import TestCase

from smi_unb.campuses.models import AdministrativeRegion, Campus
from smi_unb.buildings.forms import BuildingForm, EnableBuildingForm, \
                                    DisableBuildingForm
from smi_unb.buildings.models import Building


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    return MockResponse(None, 200)


class BuildingsFormsTest(TestCase):
    def setUp(self):
        self.building = self.create_building()

    def create_building(self):
        adm_region = AdministrativeRegion.objects.create(
            name="Test Administrative Region"
        )

        campus = Campus.objects.create(
            administrative_region=adm_region,
            name="Test Campus",
            address="Test Address",
            phone="Test Phone"
        )

        building = Building.objects.create(
            campus=campus,
            name="Test Building",
            server_ip_address="1.1.1.1"
        )

        return building

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_building_form_valid(self, mock):
        data = {
            'campus': self.building.campus.id,
            'name': 'Test Name',
            'acronym': 'Test',
            'server_ip_address': '1.2.3.4'
        }

        form = BuildingForm(
            data=data,
            campus=self.building.campus
        )

        self.assertTrue(form.is_valid())

    def test_building_form_invalid(self):
        data = {
            'campus': '',
            'name': 'Test Name',
            'acronym': 'Test',
            'server_ip_address': '1.2.3.4'
        }

        form = BuildingForm(
            data=data,
            campus=self.building.campus
        )

        self.assertFalse(form.is_valid())

    @mock.patch('requests.get', side_effect=requests.ConnectionError('Error'))
    def test_building_form_not_connecting_to_server(self, mock):
        data = {
            'campus': self.building.campus.id,
            'name': 'Test Name',
            'acronym': 'Test',
            'server_ip_address': '1.2.3.4'
        }

        form = BuildingForm(
            data=data,
            campus=self.building.campus
        )

        self.assertFalse(form.is_valid())

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_enable_building_form_valid(self, mock):
        self.building.active = False

        data = {
            'enable': ''
        }

        form = EnableBuildingForm(
            data=data,
            instance=self.building
        )

        self.assertTrue(form.is_valid())

    @mock.patch('requests.get', side_effect=requests.ConnectionError('Error'))
    def test_enable_building_not_connecting_to_server(self, mock):
        self.building.active = False

        data = {
            'enable': ''
        }

        form = EnableBuildingForm(
            data=data,
            instance=self.building
        )

        self.assertFalse(form.is_valid())

    def test_enable_building_form_invalid_fields(self):
        form = EnableBuildingForm(
            instance=self.building
        )

        self.assertFalse(form.is_valid())

    def test_enable_building_form_invalid_building_already_active(self):
        self.building.active = True

        data = {
            'enable': ''
        }

        form = EnableBuildingForm(
            data=data,
            instance=self.building
        )

        self.assertFalse(form.is_valid())

    def test_disable_building_form_valid(self):
        self.building.active = True

        data = {
            'disable': ''
        }

        form = DisableBuildingForm(
            data=data,
            instance=self.building
        )

        self.assertTrue(form.is_valid())

    def test_disable_building_form_invalid_fields(self):
        form = DisableBuildingForm(
            instance=self.building
        )

        self.assertFalse(form.is_valid())

    def test_disable_building_form_invalid_building_already_inactive(self):
        self.building.active = False

        data = {
            'disable': ''
        }

        form = DisableBuildingForm(
            data=data,
            instance=self.building
        )

        self.assertFalse(form.is_valid())
