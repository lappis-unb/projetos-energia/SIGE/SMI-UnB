FROM python:3.5

RUN apt-get update &&\
    apt-get install --no-install-recommends --no-install-suggests -y \
    \
    # Extra deps
        cron              \
        libpq-dev         \
        python3-pip       \
        postgresql        \
        postgresql-client

# Base deps
RUN pip3 install        \
    requests[security]==2.19.1  \
    django==2.0.2       \
    psycopg2==2.6.2     \
    gunicorn==19.9.0     \
    redis==2.10.3       \
    django-redis==4.3.0 \
    invoke==1.2.0      \
    numpy==1.15.2       \
    pandas==0.23.4      \
    Sphinx==1.8.1       \
    django-polymorphic==2.0.2  \
    manuel==1.9.0       \
    mpld3==0.3          \
    six==1.11.0         \
    unipath==1.1        \
    django_cron==0.5.1  \
    matplotlib==3.0.0   \
    djangorestframework==3.8.2

RUN mkdir -p /app
WORKDIR /app

COPY [ \
      "setup.py",         \
      "requirements.txt", \
      "boilerplate.ini",  \
      "manage.py",        \
      "tasks.py",         \
      "/app/"             \
]

COPY src/ /app/src/

ENV PYTHONPATH=src \
    SMI_UNB_PRODUCTION=true

# Setting cron
ADD crons/master_cron /etc/cron.d/smi-cron

RUN chmod 0644 /etc/cron.d/smi-cron

RUN touch /var/log/cron.log

RUN /usr/bin/crontab /etc/cron.d/smi-cron

# Initializing container with script
ADD scripts/start.sh /bin/start.sh

CMD /bin/bash /bin/start.sh
