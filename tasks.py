import os
import subprocess

from invoke import run, task

# Docker Tasks
@task
def install_master(ctx):
    print('Installing Master Docker')
    # Database configuration
    subprocess.call(['./scripts/db_initial_data.sh'])

    # Building docker containers
    run('docker-compose -f docker-compose-master.yml build --force-rm')

    # Initializing containers
    run('docker-compose -f docker-compose-master.yml up -d')

@task
def start_master(ctx):
    run('docker-compose -f docker-compose-master.yml start')

@task
def stop_master(ctx):
    run('docker-compose -f docker-compose-master.yml stop')

@task
def install_slave(ctx):
    print('Installing Slave Docker')
    # Database configuration
    subprocess.call(['./scripts/db_initial_data.sh'])

    # Building docker containers
    run('docker-compose -f docker-compose-slave.yml build --force-rm')

    # Initializing containers
    run('docker-compose -f docker-compose-slave.yml up -d')

@task
def start_slave(ctx):
    run('docker-compose -f docker-compose-slave.yml start')

@task
def stop_slave(ctx):
    run('docker-compose -f docker-compose-slave.yml stop')

# Django Tasks
@task
def dev(ctx):
    run('python3 manage.py runserver')

@task
def collectstatic(ctx):
    run('python3 manage.py collectstatic --noinput')

@task
def coverage(ctx, report=False):
    run('coverage run manage.py test smi_unb')

    if report:
        run('coverage report')
    else:
        run('coverage html')
        run('google-chrome htmlcov/index.html')

@task
def test(ctx):
    run('python3 manage.py test smi_unb')
